import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:tracking_realtime_location/services/geolocator_service.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GeolocatorService geoService = GeolocatorService();
  final Position _position = Position(longitude: 0.0, latitude: 0.0, timestamp: DateTime.now(), accuracy: 0.0, altitude: 0.0, heading: 0.0, speed: 0.0, speedAccuracy: 0.0);
  @override
  Widget build(BuildContext context) {
    return FutureProvider<Position>(
      create: (context) =>  geoService.getInitialLocation(),
      initialData: _position,
      child: MaterialApp(
        home: Consumer<Position>(builder: (_, position, __){
          if (position != null) return GoogleMapScreen(initialPosition: position,);
              return  Center(child: CircularProgressIndicator(),);
        },)
      ),
    );
  }
}

class GoogleMapScreen extends StatefulWidget {
  final Position? initialPosition;
  GoogleMapScreen({this.initialPosition});
  @override
  _GoogleMapScreenState createState() => _GoogleMapScreenState();
}

class _GoogleMapScreenState extends State<GoogleMapScreen> {

  final GeolocatorService _geolocatorService = GeolocatorService();
  GoogleMapController? _newGoogleMapController;
  final Completer<GoogleMapController> _controllerGM = Completer();
  Position? driverCurrentPosition;
  LocationPermission? _locationPermission;

  @override
  void initState() {
    _geolocatorService.getCurrentLocation().listen((position) {
      centerScreen(position);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context)

    );
  }
  
  Widget _buildBody(BuildContext context){
    return Stack(
      children: [
        GoogleMap(initialCameraPosition: CameraPosition(
          target: LatLng(widget.initialPosition?.latitude ?? 0.0, widget.initialPosition?.longitude ?? 0.0),
          zoom: 18,
        ),
          mapType: MapType.satellite,
          myLocationEnabled: true,
          onMapCreated: (GoogleMapController controller){
            _controllerGM.complete(controller);
          },),
        Positioned(
          top:0,
          left: 0,
          right: 0,
          child: Container(
            height: 70,
            color: Colors.pink,
            child: StreamBuilder(
              stream: _geolocatorService.getCurrentLocation(),
              builder: (BuildContext context, AsyncSnapshot<Position> snapshot) {
                if(!snapshot.hasData) return Center(child: CircularProgressIndicator(),);
                return Center(child: Text("lat:${snapshot.data?.latitude}, lng: ${snapshot.data?.longitude}"),);
              },
            ),
          )
        )
      ],
    );
  }

  Future<void> centerScreen(Position position) async{
    final GoogleMapController controllerGM = await _controllerGM.future;
    controllerGM.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(position.latitude, position.longitude),
    zoom: 18.0)));
  }


}

