import 'package:geolocator/geolocator.dart';

class GeolocatorService{

  Stream<Position> getCurrentLocation() {
    var locationSettings = LocationSettings(accuracy: LocationAccuracy.high, distanceFilter: 10 );
    return Geolocator.getPositionStream(locationSettings: locationSettings);
  }

  Future<Position> getInitialLocation() async{
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }
}